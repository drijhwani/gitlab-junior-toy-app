require_relative '../../spec_helper'

RSpec.describe 'User' do
  let!(:user) { QA::Resource::User.fabricate_via_api! }

  before do
    QA::Flow::Login.sign_in(as: QA::Runtime::User.admin)
  end

  it 'can be deleted by the admin user' do
    QA::Page::Home.perform do |home|
      home.click_account_dropdown_link
      home.click_users_link
    end

    QA::Page::User::Show.perform do |show_page|
      show_page.click_delete_for_user(user.email)
    end

    aggregate_failures do
      expect(page).to have_text('User deleted')
      expect(user.exists?).to be_falsey
    end
  end
end
