require_relative '../../spec_helper'

RSpec.describe 'User' do
  let(:name) { "John Doe #{SecureRandom.hex(8)}" }

  it 'can signup' do
    QA::Page::Home.perform(&:click_sign_up_link)

    @user = QA::Resource::User.fabricate! do |resource|
      resource.name = name
      resource.email = "johndoe-#{SecureRandom.hex(8)}@example.com"
      resource.password = 'Pa$$w0rd'
    end

    aggregate_failures do
      expect(page).to have_text('Signed up successfully')
      expect(page).to have_text(name)
    end
  end

  after do
    @user&.remove_via_api!
  end
end
