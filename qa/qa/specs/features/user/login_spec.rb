require_relative '../../spec_helper'

RSpec.describe 'User' do
  it 'can log in with valid credentials' do
    QA::Flow::Login.sign_in

    expect(page).to have_text('Logged in successfully')
  end
end
