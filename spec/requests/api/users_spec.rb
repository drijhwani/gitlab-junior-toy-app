require 'rails_helper'

RSpec.describe API::V1::Users, type: :api do
  include Rack::Test::Methods

  def app
    API::V1::Users
  end

  describe 'GET /users' do
    let!(:user) { create(:user) }
    let!(:users) { create_list(:user, 30) }

    it 'returns all users' do
      get '/api/v1/users'

      response_body = JSON.parse(last_response.body)
      expect(last_response.status).to eq(200)
      expect(response_body.count).to eq 31
      expect(response_body.first['name']).to eq(user.name)
      expect(response_body.first['email']).to eq(user.email)
    end
  end

  describe 'DELETE /user/:id' do
    let!(:user_to_delete) { create(:user) }
    let!(:user) { create(:user) }
    let!(:admin) { create(:user, :admin) }
    let(:user_personal_access_token) { create(:personal_access_token, user: user).value }

    let(:admin_personal_access_token) { create(:personal_access_token, user: admin).value }

    it 'requires a personal access token' do
      delete "/api/v1/users/#{user_to_delete.id}"

      expect(last_response.status).to eq(401)
    end

    it 'fails with a non admin personal access token' do
      delete "/api/v1/users/#{user_to_delete.id}", private_token: user_personal_access_token

      expect(last_response.status).to eq(403)
    end

    it 'succeeds with an admin personal access token' do
      delete "/api/v1/users/#{user_to_delete.id}", private_token: admin_personal_access_token

      expect(last_response.status).to eq(204)
    end
  end

  describe 'POST /users' do
    let!(:user) { create(:user) }
    let!(:admin) { create(:user, :admin) }
    let(:user_personal_access_token) { create(:personal_access_token, user: user).value }
    let(:admin_personal_access_token) { create(:personal_access_token, user: admin).value }
    let(:post_params) do
      { name: 'John doe',
        email: 'john@email.com',
        password: 'foobar123',
        password_confirmation: 'foobar123'
      }
    end

    it 'requires a personal access token' do
      post '/api/v1/users', post_params
      expect(last_response.status).to eq(401)
    end

    it 'fails with a non admin personal access token' do
      post '/api/v1/users', post_params.merge(private_token: user_personal_access_token)
      expect(last_response.status).to eq(403)
    end

    it 'succeeds with an admin personal access token' do
      post '/api/v1/users', post_params.merge(private_token: admin_personal_access_token)
      expect(last_response.status).to eq(201)
    end
  end
end
